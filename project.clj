(defproject se.jobtechdev.nlp.models/taxonomy-vec "1.2.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]

                 [com.taoensso/nippy "3.1.1"]
                 [uncomplicate/neanderthal "0.39.0"]
;;                 [org.bytedeco/mkl-platform-redist "2020.3-1.5.4"]

                 [neanderthal-stick "0.4.0"]

                 [uncomplicate/fluokitten "0.9.1"]
                 [uncomplicate/commons "0.12.0"]

                 [byte-streams "0.2.4"]

                 ]
  :main ^:skip-aot taxonomy-vec.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}}

  :deploy-repositories [
                        ["nexus-snapshots"
                         "https://nexus.jobtechdev.se/repository/maven-snapshots/"
                         ]
                        ["nexus-releases"
                         "https://nexus.jobtechdev.se/repository/maven-releases/"
                         ]
                        ]
  )
