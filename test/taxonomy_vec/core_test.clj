(ns taxonomy-vec.core-test
  (:require [clojure.test :refer :all]
            [taxonomy-vec.core :refer :all]))

(defn pi [x]
  (* Math/PI x))

(defn near? [a b]
  (< (Math/abs (- a b)) 0.01))

(deftest trigonometry-test
  (doseq [angle [(pi 0.0) (pi 0.1) (pi 1.0) (pi 0.9)]]
    (let [angle2 (-> angle cos acos)]
      (is (near? angle angle2))))
  (is (near? (acos 1.00000001)
             (acos 0.99999999)))
  (is (near? (acos -1.00000001)
             (acos -0.99999999))))

(deftest strategy-test
  (is (strategy? default-strategy))
  (is (= (decorate-with-measure default-strategy
                                {:distance Math/PI})
         {:distance Math/PI
          :cosine-similarity -1.0})))
