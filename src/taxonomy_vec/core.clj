(ns taxonomy-vec.core
  (:require
   [clojure.java.io :as io]
   [taoensso.nippy :as nippy]
   [neanderthal-stick.nippy-ext :refer [with-real-factory]]
   [clojure.java.io :as jio]
   [neanderthal-stick.core :as stick]
   [neanderthal-stick.experimental :as exp]
   [uncomplicate.neanderthal
    [core :refer :all]
    [native :refer :all]
    [linalg :as linalg]
    [auxil :as auxil]
    [vect-math :as vect-math]]

   [uncomplicate.commons.core :refer [with-release
                                      let-release
                                      release
                                      double-fn]]
   [uncomplicate.fluokitten.core :refer [fmap!
                                         fmap
                                         foldmap
                                         fold]]

   [byte-streams])

  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn load-model []
  (exp/load!
   (byte-streams/to-data-input-stream
    (jio/input-stream
     (jio/resource "models/model.nippy")))))

(defn load-concepts []
  (nippy/thaw-from-resource  "data/concepts.nippy"))

(defn load-concept-ids []
  (nippy/thaw-from-resource  "data/concept-ids.nippy"))

(def memoized-model (memoize load-model))
(def memoized-concepts (memoize load-concepts))
(def memoized-concept-ids (memoize load-concept-ids))

(def concept-id-map (into {}
                          (map-indexed (fn [i x] [x i]))
                          (memoized-concept-ids)))

(defn cosine-similarity
  "Returns the cosine similarity between two vectors"
  [u v]
  (let [dot-product (dot u v)
        norm2u (nrm2 u)
        norm2v (nrm2 v)]
    (if (some zero? [dot-product norm2u norm2v])
      0.0
      (/  dot-product norm2u norm2v))))

(def acos-bound 1.001)

;; Robust trigonometry functions that tolerate a little bit of numeric error.
(defn cos
  "The cos function, with extra checking."
  [x]
  (assert (Double/isFinite x))
  (Math/cos x))

(defn acos
  "The arccos function, tolerant to some numeric deviations."
  [x]
  (assert (<= (- acos-bound) x))
  (assert (<= x acos-bound))
  (cond
    (< x -1) Math/PI
    (< 1 x) 0
    :default (Math/acos x)))

(defn angle
  "Compute the angle, in radians, between two vectors."
  [u v]
  (acos (cosine-similarity u v)))

(defn angle-aggregation
  "Compute the angle from every vector in `ref-vectors` to `x` and aggregate the angles using the `aggregator` function. Suitable aggregators would for instance be the sum of angles, sum of squared angles or maximum angle."
  [aggregator ref-vectors x]
  (apply aggregator (map #(angle % x) ref-vectors)))

(defn average-angle
  "Compute the average angle"
  [ref-vectors x]
  (/ (angle-aggregation + ref-vectors x)
     (count ref-vectors)))

(defn- merge-with-data [index-and-distance all-concepts]
  (merge (nth all-concepts (:index index-and-distance))
         (dissoc index-and-distance :index)))

(defn get-related-concepts-distance [distance-function model]
  {:pre [(fn? distance-function)]}
  (let [vecs (cols model)
        vec-index-and-distance (map-indexed
                                (fn [i v]
                                  {:index i
                                   :distance (distance-function v)})
                                vecs)
        related (sort-by :distance vec-index-and-distance)
        related-100 (take 100 related)]
    (map #(merge-with-data % (memoized-concepts)) related-100)))

(defn strategy
  "A strategy is the algorithm used to rank a concept vector against some query vectors."
  [measure-key measure-fn distance-function]
  {:pre [(keyword? measure-key)
         (fn? measure-fn)
         (fn? distance-function)]}
  {:measure-key measure-key
   :measure-fn measure-fn
   :strategy-fn (fn [query-vectors]
                  (fn [x]
                    (distance-function query-vectors x)))})

(defn strategy?
  "Test if something is a strategy."
  [x]
  (and (map? x)
       (keyword (:measure-key x))))

(defn decorate-with-measure
  "This function converts the distance of a `concept` to the specified measure of the `strategy` and returns a new concept with that measure."
  [strategy concept]
  (assoc concept
         (:measure-key strategy)
         ((:measure-fn strategy)
          (:distance concept))))

(defn distance-fn [strategy query-vectors]
  ((:strategy-fn strategy) query-vectors))

;; This strategy returns a cosine similarity from the average angle
;; between the query vectors and the input vector
(def average-angle-cosine-strategy
  (strategy :cosine-similarity cos average-angle))

(def default-strategy average-angle-cosine-strategy)

(defn get-related-concepts-by-ids
  "Return the concepts related to query concepts with `ids`. The `strategy` parameter determines how the closest concepts are ranked."
  [strategy ids]
  {:pre [(strategy? strategy)
         (coll? ids)
         (every? string? ids)]}
  (let [concept-matrix (memoized-model)
        query-vectors (for [id ids]
                        (col concept-matrix (get concept-id-map id)))
        distance-fn (distance-fn strategy query-vectors)]
    (map
     #(decorate-with-measure strategy %)
     (get-related-concepts-distance distance-fn concept-matrix))))

(defn get-related-concepts-by-id
  "Get the concepts closest to a query concept with the provided `id`."
  [id]
  (get-related-concepts-by-ids average-angle-cosine-strategy [id]))



(comment

  (def data (get-related-concepts-by-id "n1YH_eKo_mvR"))
  
  )
