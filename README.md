# taxonomy-vec

This library is a helper library for distributing large vector model files via nexus. 
It also acts as a wrapper with helper functions to lookup relations in the vector models with cosine similarity.


## Installation

No matrices included in the resource folder, since they are too big!

You build the files `concepts.nippy` `model.nippy` `morpheme-corpus.nippy` with `jobtech_nlp_word_embeddings` library and copy them into the resource folder.

Then use `lein deploy` to deploy jars to nexus.

First you need to add `profiles.clj` in your .lein folder first with login information to nexus.

``` clojure
{:user {
        :repositories [["snapshots" {:id "nexus-snapshots"
                                     :url "https://nexus.jobtechdev.se/repository/maven-snapshots/"}]
                       ["releases" {:id "nexus-releases"
                                    :url "https://nexus.jobtechdev.se/repository/maven-releases/"
                                    :sign-releases false}]]}

 :auth {:repository-auth {#"jobtechdev"      {:username "YOUR_USERNAME"
                                              :password "YOUR_PASSWORD"}}}}

```


## Usage

FIXME: explanation

    $ java -jar taxonomy-vec-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
